# Utilise une image Python officielle
FROM python:3.9-slim

# Définir le répertoire de travail
WORKDIR /app

# Copier les fichiers de l'application
COPY app.py /app/
COPY requirements.txt /app/
COPY templates /app/templates/
COPY tests /app/tests/

# Installer les dépendances Python
RUN pip install --no-cache-dir -r requirements.txt

# Installer les outils Kubernetes
RUN apt-get update && apt-get install -y apt-transport-https gnupg2 curl \
    && curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - \
    && echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list \
    && apt-get update \
    && apt-get install -y kubectl

# Exposer le port que l'application va utiliser
EXPOSE 5000

# Démarrer l'application Flask
CMD ["python", "app.py"]
