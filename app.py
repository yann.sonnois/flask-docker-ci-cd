from flask import Flask, render_template

app = Flask(__name__)

# Route pour la page d'accueil
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/contact')
def contact():
    return render_template('contact.html')

# Ajoutez d'autres routes et fonctions selon les besoins de votre application

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')